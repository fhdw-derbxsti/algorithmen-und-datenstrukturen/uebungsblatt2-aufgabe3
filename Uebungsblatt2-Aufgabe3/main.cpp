﻿#include <iostream>

using namespace std;

int aufrufZaehler = 0;

// Rekursive Funktion zur Berechnung von x^n
double berechnePotenz(int x, int n) {
    // Inkrementiere den Zähler bei jedem rekursiven Aufruf
    aufrufZaehler++;
    
    // Basisfall: Wenn n gleich 0 ist, ist das Ergebnis 1.
    if (n == 0) {
        return 1;
    }
    // Wenn n gerade ist
    if (n % 2 == 0) {
        double temp = berechnePotenz(x, n / 2);
        return temp * temp;
    } else {
        // Wenn n ungerade ist
        double temp = berechnePotenz(x, (n - 1) / 2);
        return temp * temp * x;
    }
}

// Funktion zur Berechnung von x^-n
double berechneNegativePotenz(int x, int n) {
    if (n < 0) {
        // Verwende den Algorithmus für den positiven Exponenten und invertiere das Ergebnis
        double positiveErgebnis = berechnePotenz(x, -n);
        return 1.0 / positiveErgebnis;
    } else {
        // Für nicht-negative Exponenten verwende den ursprünglichen Algorithmus
        return berechnePotenz(x, n);
    }
}

int main() {
    int x = 2; // Basis
    int n = 47; // Exponent

    aufrufZaehler = 0; // Setze den Aufrufzähler zurück

    double ergebnis = berechneNegativePotenz(x, n);

    cout << x << "^" << n << " = " << ergebnis << endl;
    cout << "Anzahl der rekursiven Aufrufe: " << aufrufZaehler << endl;

    return 0;
}